# License

Standard license files used by the Drupal Infrastructure team for repositories supporting Drupal.org, it's subsites, infrastructure, as well as our self-hosted GitLab instance at [git.drupalcode.org](git.drupalcode.org). 

Drupal, and all contributed files that are derivative works of Drupal hosted on Drupal.org, are licensed under the [GNU General Public License, version 2 or later](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html). That means you are free to download, reuse, modify, and distribute any files hosted in Drupal.org's Git repositories under the terms of either the GPL version 2 or version 3, and to run Drupal in combination with any code with any license that is compatible with either versions 2 or 3, such as the Affero General Public License (AGPL) version 3.

Source: https://www.drupal.org/about/licensing#drupal-license
